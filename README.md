# README #

This script is to match up users for secret santas.

### What is this repository for? ###

* Matching up secret santas, of course!

### How do I get set up? ###

* Package requirements:
    * python3.x
    * random
    * csv
    * smtplib
* Files requirements:
    * hohoho.py
        * main file
    * list
        * tab-separated list of users
        * See headers for more information
    * gifteelog
        * File created by the script (recreated each time) with a mapping of santa to giftee
* List Headers:
    * nick - Name or nickname user goes by (Required)
    * email - User's email address (Required)
    * prefs - User preferences, likes/dislikes, notes, wishlists, etc
    * addresses - User's mailing address
    * nomatch - Nick of user NOT to be matched with. IE Should *not* have this user as a giftee

### Who do I talk to? ###

* Nobody, go away