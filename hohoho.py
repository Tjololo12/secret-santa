#!/usr/bin/env python
#-*- coding: utf-8 -*-

import random
import smtplib
import csv
server = None


def send_email(recipient, subject, body):
    global server
    FROM = ""
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body
    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server.sendmail(FROM, TO, message)
        print('successfully sent mail to ' + str(TO))
    except Exception as ex:
        print('failed to send mail to ' + str(TO))


def parseList():
    csvfile = open('list', 'r')
    fieldnames = ("nick","email","prefs","addresses","nomatch")
    reader = csv.DictReader(csvfile, fieldnames, delimiter='\t')
    data = [row for row in reader]
    data.pop(0)
    return data


def getMatches(userList):
    santaList = list(userList)
    gifteeList = list(userList)
    matches = []
    while len(gifteeList) > 0:
        random.shuffle(gifteeList)
        random.shuffle(santaList)
        giftee = gifteeList.pop()
        avoidUser = giftee["nomatch"]
        if avoidUser:
            if len(santaList) == 1:
                print(santaList)
                print(gifteeList)
                print("WHY DO THEY HATE EACH OTHER?!")
                return getMatches(userList)
            while santaList[0]["nick"].lower() == avoidUser.lower():
                random.shuffle(santaList)
        if santaList[0]["nick"] == giftee["nick"]:
            if len(santaList) == 1:
                print(santaList)
                print(gifteeList)
                print(userList)
                print("OH GOD NO IT HAPPENED")
                return getMatches(userList)
            while santaList[0]["nick"] == giftee["nick"]:
                random.shuffle(santaList)
        santa = santaList.pop(0)
        matches.append((santa, giftee))
    return matches


def log(message):
    with open("gifteelog", 'a') as logfile:
        logfile.write(message)


def matchUsers(santa, giftee):
    beginning = "HO HO HO! Hello " + santa["nick"] + "! It's your friendly neighborhood Santa Claus here! I'm sure you've been a good little biped. " \
              "Or not, I don't really care. Regardless, you've entered into the #mturk secret santa, and may god " \
              "have mercy on our souls. Your giftee information is below! Remember, you need to mail your gift(s) by " \
              "12/20, and there is a $10 price guide. You can buy/make something worth more, but that's the price to shoot " \
              "for. If there is no address listed, you'll have to contact the giftee for information about shipping " \
              "or for an amazon gift list. If you want to remain secret, you can use http://www.anonymousfeedback.net/ to " \
              "send emails anonymously. I tested it with Pineapple, it works really well. Without further ado, here is your "\
              " secret santa match! Thank you for participating!\n\n"
    ending = "\n\nThanks again for being a part of this. Make sure to keep your santa a secret! Leave it up to the giftee " \
           "if they want to expose who the santa is. I recommend putting your name in a note/card to your giftee if you " \
           "want them to know who you are, and let them share what they got and who you are. If you don't want them to " \
           "share, make sure you let them know. Now if you'll excuse me, Rudolph just got back from rehab and Dasher is " \
           "trying to challenge him to a drinking competition. Happy holidays!\n~Santa"
    subject = "IS THAT JINGLE BELLS I HEAR?!?!"
    message = "Nick: "+str(giftee["nick"])+"\nPreferences: "+str(giftee["prefs"])+"\nEmail address: "+str(giftee["email"])+"\nMailing Address: "+str(giftee["addresses"])
    send_email(santa["email"], subject, beginning + message + ending)

if __name__ == "__main__":
    with open('gifteelog', 'w'):
        pass
    gmail_user = ''
    gmail_pwd = ''
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(gmail_user, gmail_pwd)
    userList = parseList()
    matches = getMatches(userList)
    for match in matches:
        log(match[0]["nick"] + " giving to " + match[1]["nick"] + "\n")
        matchUsers(match[0], match[1])
    server.close()
